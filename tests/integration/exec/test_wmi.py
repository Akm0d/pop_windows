import pytest


@pytest.mark.asyncio
class TestWMI:
    async def test_get(self, hub, subtests):
        for class_ in hub.exec.wmi.WMI.classes:
            with subtests.test(class_=class_):
                pytest.skip("TODO This test is extremely expensive")
                c = await hub.exec.wmi.get(class_)
                if isinstance(c, list):
                    if not len(c) == 1:
                        continue
                    for prop in c[0].properties:
                        with subtests.test(property_=prop):
                            await hub.exec.wmi.get(class_, 0, prop)
                for prop in c.properties():
                    with subtests.test(property_=prop):
                        await hub.exec.wmi.get(class_, prop)
