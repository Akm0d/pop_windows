import pytest
from typing import List


@pytest.fixture(scope="function")
async def temp_key(hub):
    subkey = "Software\\MyKey\\TEST_SUB_KEY"
    # Verify Key doesn't already exist
    assert not await hub.exec.reg.check_access(subkey)
    # Create the key
    await hub.exec.reg.create_key(subkey)
    # Verify the key exists
    assert await hub.exec.reg.check_access(subkey)
    # Pass the key name to function
    yield subkey
    # Delete the key
    await hub.exec.reg.delete_key(subkey)
    # verify that it doesn't exist
    assert not await hub.exec.reg.check_access(subkey)


@pytest.mark.asyncio
class TestReg:
    name = "Test Value Name"

    async def test_binary_value(self, hub, temp_key):
        value = b"123"
        await hub.exec.reg.set_binary_value(temp_key, value, self.name)
        ret = await hub.exec.reg.get_binary_value(temp_key, self.name)
        assert ret == value

    async def test_dword_value(self, hub, temp_key):
        value = 1234
        await hub.exec.reg.set_dword_value(temp_key, value, self.name)
        ret = await hub.exec.reg.get_dword_value(temp_key, self.name)
        assert ret == value

    async def test_qword_value(self, hub, temp_key):
        value = 1234
        await hub.exec.reg.set_qword_value(temp_key, value, self.name)
        ret = await hub.exec.reg.get_qword_value(temp_key, self.name)
        assert ret == value

    async def test_expanded_string_value(self, hub, temp_key):
        value = "%systemroot%"
        await hub.exec.reg.set_expanded_string_value(temp_key, value, self.name)
        return
        ret = await hub.exec.reg.get_expanded_string_value(temp_key, self.name)
        # TODO this will be the expanded system root
        assert ret == value

    async def test_multi_string_value(self, hub, temp_key):
        return
        value = "A super cool value"
        await hub.exec.reg.set_multi_string_value(temp_key, value, self.name)
        ret = await hub.exec.reg.get_multi_string_value(temp_key, self.name)
        assert ret == value

    async def test_string_value(self, hub, temp_key):
        value = "A super cool value"
        await hub.exec.reg.set_string_value(temp_key, value, self.name)
        return
        ret = await hub.exec.reg.get_string_value(temp_key, self.name)
        assert ret == value

    async def test_security_descriptor(self, hub, temp_key):
        ret = await hub.exec.reg.get_security_descriptor(temp_key)
        assert isinstance(ret, int)

    async def test_keys(self, hub):
        ret = await hub.exec.reg.keys("SYSTEM")
        assert isinstance(ret, list)

    async def test_values(self, hub):
        names, types = await hub.exec.reg.values("SYSTEM")
        assert isinstance(names, list)
        assert isinstance(types, list)
