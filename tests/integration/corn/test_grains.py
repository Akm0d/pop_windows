import pytest


class TestGrains:
    @pytest.mark.asyncio
    async def test_grains_values(self, hub):
        """
        Verify that a standard set of grains have been defined
        """
        missing_grains = {
            "biosversion",
            "cpu_model",
            "cpuarch",
            "cwd",
            "domain",
            "fqdn",
            "fqdn_ip4",
            "fqdn_ip6",
            "fqdns",
            "hardware_virtualization",
            "host",
            "hw_addr_interfaces",
            "init",
            "ip4_interfaces",
            "ip6_interfaces",
            "ip_interfaces",
            "ipv4",
            "ipv6",
            "kernel",
            "kernelrelease",
            "kernelversion",
            "locale_info",
            "localhost",
            "manufacturer",
            "mem_total",
            "motherboard",
            "nodename",
            "os",
            "os_family",
            "os_release",
            "osfinger",
            "osfullname",
            "osmanufacturer",
            "osrelease_info",
            "osversion",
            "path",
            "productname",
            "ps",
            "pythonexecutable",
            "pythonpath",
            "serialnumber",
            "timezone",
            "virtual",
            "serialnumber",
            "windowsdomain",
            "windowsdomaintype",
        } - set(hub.grains.GRAINS.keys())
        assert not missing_grains, "Essential grains are missing: {}".format(
            ",".join(missing_grains)
        )

    @pytest.mark.asyncio
    async def test_grains(self, hub, subtests):
        """
        Document the grains that have no values
        """
        for grain, value in hub.grains.GRAINS.items():
            with subtests.test(grain=grain):
                if not (value or isinstance(value, int) or isinstance(value, bool)):
                    pytest.skip(f'"{grain}" does not have a value')
