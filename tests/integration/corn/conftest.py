import pop.hub
import pytest


@pytest.fixture(scope="function")
async def hub() -> pop.hub.Hub:
    hub = pop.hub.Hub()

    # Load subs
    hub.pop.sub.add(dyne_name="exec")
    hub.pop.sub.add(dyne_name="grains")

    await hub.grains.init.collect()

    yield hub
