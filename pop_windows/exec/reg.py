"""
Functions ported from salt.utils.win_reg
"""
import ctypes
import logging
import win32con
import wmi
from typing import Any, Dict, List, Tuple

log = logging.getLogger(__name__)

HKEYS = {
    "HKEY_CURRENT_CONFIG": win32con.HKEY_CURRENT_CONFIG,
    "HKEY_CLASSES_ROOT": win32con.HKEY_CLASSES_ROOT,
    "HKEY_CURRENT_USER": win32con.HKEY_CURRENT_USER,
    "HKEY_LOCAL_MACHINE": win32con.HKEY_LOCAL_MACHINE,
    "HKEY_USERS": win32con.HKEY_USERS,
}


def __init__(hub):
    # TODO Read some of these WMI options from hub.OPTS
    """
    computer=None,
    impersonation_level="Impersonate",
    authentication_level="Default",
    authority=None,
    privileges=None,
    moniker=None
    """
    hub.exec.reg.REGISTRY = wmi.WMI(namespace="DEFAULT").StdRegProv
    hub.exec.reg.HKEYS = HKEYS

    hub.exec.reg.VTYPE = {
        "REG_BINARY": win32con.REG_BINARY,
        "REG_DWORD": win32con.REG_DWORD,
        "REG_EXPAND_SZ": win32con.REG_EXPAND_SZ,
        "REG_MULTI_SZ": win32con.REG_MULTI_SZ,
        "REG_SZ": win32con.REG_SZ,
        "REG_QWORD": win32con.REG_QWORD,
    }

    hub.exec.reg.OPTTYPE = {"REG_OPTION_NON_VOLATILE": 0, "REG_OPTION_VOLATILE": 1}

    # delete_key_recursive uses this to check the subkey contains enough \
    # as we do not want to remove all or most of the registry
    hub.exec.reg.SUBKEY_SLASH_CHECK = {
        win32con.HKEY_CURRENT_USER: 0,
        win32con.HKEY_LOCAL_MACHINE: 1,
        win32con.HKEY_USERS: 1,
        win32con.HKEY_CURRENT_CONFIG: 1,
        win32con.HKEY_CLASSES_ROOT: 1,
    }

    hub.exec.reg.REGISTRY_32 = {
        True: win32con.KEY_READ | win32con.KEY_WOW64_32KEY,
        False: win32con.KEY_READ,
    }


async def _normalize_hkey(hkey: str) -> str:
    if hkey in HKEYS.values():
        return hkey
    return HKEYS[hkey.upper()]


async def check_access(
    hub, subkey: str, hive: str = win32con.HKEY_LOCAL_MACHINE, required: int = 3
) -> bool:
    """
    hDefKey [in]

        A registry tree, also known as a hive, that contains the sSubKeyName path. The default value is HKEY_LOCAL_MACHINE.

        The following trees are defined in WinReg.h.

    HKEY_CLASSES_ROOT (2147483648)

    HKEY_CURRENT_USER (2147483649)

    HKEY_LOCAL_MACHINE (2147483650)

    HKEY_USERS (2147483651)

    HKEY_CURRENT_CONFIG (2147483653)

    sSubKeyName [in]

        The key to be verified.

    uRequired [in]

        A parameter that specifies the access permissions to be verified. You can add these values together to verify more than one access permission. The default value is 3, KEY_QUERY_VALUE plus KEY_SET_VALUE. The following access permission values are defined in WinNT.h.

    KEY_QUERY_VALUE (1)

        Required to query the values of a registry key.

    KEY_SET_VALUE (2)

        Required to create, delete, or set a registry value.

    3 (KEY_QUERY_VALUE | KEY_SET_VALUE)

        Default value, allows querying, creating, deleting, or setting a registry value.

    KEY_CREATE_SUB_KEY (4)

        Required to create a subkey of a registry key.

    KEY_ENUMERATE_SUB_KEYS (8)

        Required to enumerate the subkeys of a registry key.

    KEY_NOTIFY (16)

        Required to request change notifications for a registry key or for subkeys of a registry key.

    KEY_CREATE (32)

        Required to create a registry key.

    DELETE (65536)

        Required to delete a registry key.

    READ_CONTROL (131072)

        Combines the STANDARD_RIGHTS_READ, KEY_QUERY_VALUE, KEY_ENUMERATE_SUB_KEYS, and KEY_NOTIFY values.

    WRITE_DAC (262144)

        Required to modify the DACL in the object's security descriptor.

    WRITE_OWNER (524288)

        Required to change the owner in the object's security descriptor.

    bGranted [out]

        If true, the user has the specified access permissions.

    """
    ret, status = hub.exec.reg.REGISTRY.CheckAccess(
        await _normalize_hkey(hive), subkey, required
    )
    if status:
        log.error(ctypes.WinError(status))
    return ret


async def create_key(hub, subkey: str, hive: str = win32con.HKEY_LOCAL_MACHINE):
    status = hub.exec.reg.REGISTRY.CreateKey(await _normalize_hkey(hive), subkey)[0]
    assert not status, ctypes.WinError(status)


async def delete_key(hub, subkey: str, hive: str = win32con.HKEY_LOCAL_MACHINE):
    """
    hDefKey [in]

        A registry tree, also known as a hive, that contains the sSubKeyName path. The default value is HKEY_LOCAL_MACHINE.

        The following trees are defined in WinReg.h.

    HKEY_CLASSES_ROOT (2147483648)

    HKEY_CURRENT_USER (2147483649)

    HKEY_LOCAL_MACHINE (2147483650)

    HKEY_USERS (2147483651)

    HKEY_CURRENT_CONFIG (2147483653)

    sSubKeyName [in]

        The key to be deleted.
"""
    status = hub.exec.reg.REGISTRY.DeleteKey(await _normalize_hkey(hive), subkey)[0]
    assert not status, ctypes.WinError(status)


async def del_value(
    hub, subkey: str, value_name: str = "", hive: str = win32con.HKEY_LOCAL_MACHINE
):
    """
    hDefKey [in]

        A registry tree, also known as a hive, that contains the sSubKeyName path. The default value is HKEY_LOCAL_MACHINE.

        The following trees are defined in WinReg.h.

    HKEY_CLASSES_ROOT (2147483648)

    HKEY_CURRENT_USER (2147483649)

    HKEY_LOCAL_MACHINE (2147483650)

    HKEY_USERS (2147483651)

    HKEY_CURRENT_CONFIG (2147483653)

    sSubKeyName [in]

        A key that contains the named value to be deleted.

    sValueName [in]

        The named value to be deleted from the subkey. Specify an empty string to delete the default named value. The default named value is not deleted. The value is set to the following: value not set.
    """
    return hub.exec.reg.REGISTRY.DeleteValue(hive, subkey, value_name)


async def get_binary_value(
    hub, subkey: str, value_name: str = "", hive: str = win32con.HKEY_LOCAL_MACHINE,
) -> bytes:
    """
    hDefKey [in]

        A registry tree, also known as a hive, that contains the sSubKeyName path. The default value is HKEY_LOCAL_MACHINE.

        The following trees are defined in WinReg.h.

    HKEY_CLASSES_ROOT (2147483648)

    HKEY_CURRENT_USER (2147483649)

    HKEY_LOCAL_MACHINE (2147483650)

    HKEY_USERS (2147483651)

    HKEY_CURRENT_CONFIG (2147483653)

    sSubKeyName [in]

        A path that contains the named values.

    sValueName [in]

        A named value whose data value you are retrieving. Specify an empty string to get the default named value.

    uValue [out]

        An array of binary bytes.
"""
    status, ret = hub.exec.reg.REGISTRY.GetBinaryValue(
        await _normalize_hkey(hive), subkey, value_name
    )
    assert not status, ctypes.WinError(status)
    return bytes(ret)


async def get_dword_value(
    hub, subkey: str, value_name: str = "", hive: str = win32con.HKEY_LOCAL_MACHINE,
) -> int:
    status, ret = hub.exec.reg.REGISTRY.GetDWORDValue(
        await _normalize_hkey(hive), subkey, value_name
    )
    assert not status, ctypes.WinError(status)
    return int(ret)


async def get_qword_value(
    hub, subkey: str, value_name: str = "", hive: str = win32con.HKEY_LOCAL_MACHINE,
) -> int:
    status, ret = hub.exec.reg.REGISTRY.GetQWORDValue(
        await _normalize_hkey(hive), subkey, value_name
    )
    assert not status, ctypes.WinError(status)
    return int(ret)


async def get_expanded_string_value(
    hub, subkey: str, value_name: str = "", hive: str = win32con.HKEY_LOCAL_MACHINE,
) -> str:
    status, ret = hub.exec.reg.REGISTRY.GetExpandedStringValue(
        await _normalize_hkey(hive), subkey, value_name
    )
    assert not status, ctypes.WinError(status)
    return ret


async def get_multi_string_value(
    hub,
    subkey: str,
    value: str,
    value_name: str = "",
    hive: str = win32con.HKEY_LOCAL_MACHINE,
) -> str:
    status, ret = hub.exec.reg.REGISTRY.GetMultiStringValue(
        await _normalize_hkey(hive), subkey, value_name, value
    )
    assert not status, ctypes.WinError(status)
    return ret


async def get_security_descriptor(
    hub, subkey: str, hive: str = win32con.HKEY_LOCAL_MACHINE
) -> int:
    """
    hDefKey [in]

        A registry tree, also known as a hive, that contains the sSubKeyName path. The default value is HKEY_LOCAL_MACHINE.

        The following trees are defined in WinReg.h.

    HKEY_CLASSES_ROOT (2147483648)

    HKEY_CURRENT_USER (2147483649)

    HKEY_LOCAL_MACHINE (2147483650)

    HKEY_USERS (2147483651)

    HKEY_CURRENT_CONFIG (2147483653)

    sSubKeyName [in]

        The name of the registry key that has the security descriptor.

    Descriptor [out]

        The security descriptor from the key.
    """
    status, ret = hub.exec.reg.REGISTRY.GetSecurityDescriptor(
        await _normalize_hkey(hive), subkey
    )
    assert not status, ctypes.WinError(status)
    return ret


async def get_string_value(
    hub, subkey: str, value_name: str = "", hive: str = win32con.HKEY_LOCAL_MACHINE,
) -> str:
    status, ret = hub.exec.reg.REGISTRY.GetStringValue(
        hDefKey=await _normalize_hkey(hive), sSubKeyName=subkey, sValueName=value_name
    )
    assert not status, ctypes.WinError(status)
    return ret


async def set_binary_value(
    hub,
    subkey: str,
    value: bytes,
    value_name: str = "",
    hive: str = win32con.HKEY_LOCAL_MACHINE,
):
    status = hub.exec.reg.REGISTRY.SetBinaryValue(
        await _normalize_hkey(hive), subkey, value_name, value
    )[0]
    assert not status, ctypes.WinError(status)


async def set_dword_value(
    hub,
    subkey: str,
    value: int,
    value_name: str = "",
    hive: str = win32con.HKEY_LOCAL_MACHINE,
):
    status = hub.exec.reg.REGISTRY.SetDWORDValue(
        await _normalize_hkey(hive), subkey, value_name, value
    )[0]
    assert not status, ctypes.WinError(status)


async def set_qword_value(
    hub,
    subkey: str,
    value: int,
    value_name: str = "",
    hive: str = win32con.HKEY_LOCAL_MACHINE,
):
    status = hub.exec.reg.REGISTRY.SetQWORDValue(
        await _normalize_hkey(hive), subkey, value_name, value
    )[0]
    assert not status, ctypes.WinError(status)


async def set_expanded_string_value(
    hub,
    subkey: str,
    value: str,
    value_name: str = "",
    hive: str = win32con.HKEY_LOCAL_MACHINE,
):
    status = hub.exec.reg.REGISTRY.SetExpandedStringValue(
        await _normalize_hkey(hive), subkey, value_name, value
    )[0]
    assert not status, ctypes.WinError(status)


async def set_multi_string_value(
    hub,
    subkey: str,
    value: str,
    value_name: str = "",
    hive: str = win32con.HKEY_LOCAL_MACHINE,
):
    status = hub.exec.reg.REGISTRY.SetMultiStringValue(
        await _normalize_hkey(hive), subkey, value_name, value
    )[0]
    assert not status, ctypes.WinError(status)


async def set_string_value(
    hub,
    subkey: str,
    value: str,
    value_name: str = "",
    hive: str = win32con.HKEY_LOCAL_MACHINE,
):
    status = hub.exec.reg.REGISTRY.SetStringValue(
        await _normalize_hkey(hive), subkey, value_name, value
    )[0]
    assert not status, ctypes.WinError(status)


async def set_security_descriptor(
    hub, subkey: str, security_descriptor: int, hive: str = win32con.HKEY_LOCAL_MACHINE
):
    status = hub.exec.reg.REGISTRY.SetSecurityDescriptor(
        hive, subkey, security_descriptor
    )[0]
    assert not status, ctypes.WinError(status)


async def keys(hub, subkey: str, hive: str = win32con.HKEY_LOCAL_MACHINE) -> List[str]:
    """
    hive [in]

        A registry tree, also known as a hive, that contains the sSubKeyName path. The default value is HKEY_LOCAL_MACHINE.

        The following trees are defined in WinReg.h.

    HKEY_CLASSES_ROOT (2147483648)

    HKEY_CURRENT_USER (2147483649)

    HKEY_LOCAL_MACHINE (2147483650)

    HKEY_USERS (2147483651)

    HKEY_CURRENT_CONFIG (2147483653)

    sSubKey[in]

        A path that contains the subkeys to be enumerated.

    sNames [out]

        An array of subkey strings.
    """
    status, ret = hub.exec.reg.REGISTRY.EnumKey(await _normalize_hkey(hive), subkey)
    assert not status, ctypes.WinError(status)
    return ret


async def values(
    hub, subkey: str, hive=win32con.HKEY_LOCAL_MACHINE
) -> Tuple[List[str], List[str]]:
    """
    hive [in]

        A registry tree, also known as a hive, that contains the sSubKeyName path. The default value is HKEY_LOCAL_MACHINE.

        The following trees are defined in WinReg.h.

    HKEY_CLASSES_ROOT (2147483648)

    HKEY_CURRENT_USER (2147483649)

    HKEY_LOCAL_MACHINE (2147483650)

    HKEY_USERS (2147483651)

    HKEY_CURRENT_CONFIG (2147483653)

    HKEY_DYN_DATA (2147483654)

    sSubKey[in]

        A path that contains the named values to be enumerated.

    sNames [out]

        An array of named value strings. The elements of this array correspond directly to the elements of the Types parameter. Returns null if only the default value is available.

    Types [out]

        An array of data value types (integers). You can use these types to determine which of the several Get methods to call. For example, if the data value type is REG_SZ, you call the GetStringValue method to retrieve the named value's data value. The elements of this array correspond directly with the elements of the sNames parameter.

        The following data value types are defined in WinNT.h:

            REG_SZ (1)
            REG_EXPAND_SZ (2)
            REG_BINARY (3)
            REG_DWORD (4)
            REG_MULTI_SZ (7)
            REG_QWORD (11)

        Returns null if only the default value is available.

    """
    status, names, types = hub.exec.reg.REGISTRY.EnumValues(
        await _normalize_hkey(hive), subkey
    )
    assert not status, ctypes.WinError(status)
    return names, types
