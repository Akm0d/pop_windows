import platform


async def load_uname(hub):
    """
    Verify that POP linux is running on windows
    """
    (
        hub.corn.CORN["kernel"],
        hub.corn.CORN["nodename"],
        hub.corn.CORN["kernelrelease"],
        hub.corn.CORN["kernelversion"],
        hub.corn.CORN["cpuarch"],
        hub.corn.CORN["cpu_model"],
    ) = platform.uname()

    assert (
        hub.corn.CORN["kernel"] == "Windows"
    ), "POP-Windows is only intended for Windows systems"


async def load_os(hub):
    """
    Hard coded corn for windows systems
    """
    hub.corn.CORN["init"] = "Windows"
    hub.corn.CORN["os"] = "Windows"
    hub.corn.CORN["os_family"] = "Windows"
    hub.corn.CORN["ps"] = "tasklist.exe"
