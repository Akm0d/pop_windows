# Provides:
#    kernelrelease
#    osversion
#    osrelease
#    osservicepack
#    osmanufacturer
#    manufacturer
#    productname
#    biosversion
#    serialnumber
#    osfullname
#    timezone
#    windowsdomain
#    windowsdomaintype
#    motherboard.productname
#    motherboard.serialnumber
#    virtual
import logging
import re

log = logging.getLogger(__name__)


async def _clean_value(key: str, val: str) -> str or None:
    """
    Clean out well-known bogus values.
    If it isn't clean (for example has value 'None'), return None.
    Otherwise, return the original value.

    NOTE: This logic also exists in the smbios module. This function is
          for use when not using smbios to retrieve the value.
    """
    if val is None or not val or re.match("none", val, flags=re.IGNORECASE):
        return None
    elif re.search("serial|part|version", key):
        # 'To be filled by O.E.M.
        # 'Not applicable' etc.
        # 'Not specified' etc.
        # 0000000, 1234567 etc.
        # begone!
        if (
            re.match(r"^[0]+$", val)
            or re.match(r"[0]?1234567[8]?[9]?[0]?", val)
            or re.search(
                r"sernum|part[_-]?number|specified|filled|applicable",
                val,
                flags=re.IGNORECASE,
            )
        ):
            return None
    elif re.search("asset|manufacturer", key):
        # AssetTag0. Manufacturer04. Begone.
        if re.search(
            r"manufacturer|to be filled|available|asset|^no(ne|t)",
            val,
            flags=re.IGNORECASE,
        ):
            return None
    else:
        # map unspecified, undefined, unknown & whatever to None
        if re.search(r"to be filled", val, flags=re.IGNORECASE) or re.search(
            r"un(known|specified)|no(t|ne)? (asset|provided|defined|available|present|specified)",
            val,
            flags=re.IGNORECASE,
        ):
            return None
    return val


async def load_os_info(hub):
    # https://msdn.microsoft.com/en-us/library/aa394239(v=vs.85).aspx
    osinfo = await hub.exec.wmi.get("Win32_OperatingSystem", 0)
    hub.corn.CORN["kernelrelease"] = await _clean_value("kernelrelease", osinfo.Version)
    hub.corn.CORN["osversion"] = await _clean_value("osversion", osinfo.Version)
    hub.corn.CORN["osmanufacturer"] = await _clean_value(
        "osmanufacturer", osinfo.Manufacturer
    )
    hub.corn.CORN["osfullname"] = await _clean_value("osfullname", osinfo.Caption)
