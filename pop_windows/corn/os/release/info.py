async def load_osrelease_info(hub):
    os_release = hub.corn.CORN["os_release"]
    if "Server" in os_release:
        hub.corn.CORN["osrelease_info"] = tuple(os_release.split("Server", 1))
        hub.corn.CORN["osrelease_info"][1] = hub.grais.CORN["osrelease_info"][1].lstrip(
            "R"
        )
    else:
        hub.corn.CORN["osrelease_info"] = os_release.split(".")

    hub.corn.CORN["osfinger"] = "{os}-{ver}".format(
        os=hub.corn.CORN["os"], ver=os_release
    )
