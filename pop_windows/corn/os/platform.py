import platform


async def load_kernel_version(hub):
    hub.corn.CORN["kernelversion"] = platform.version()
