import ctypes.wintypes
from typing import Any, Dict

# TODO This can all be greatly simplified,
# also it looks like more corn can be derived from this info than were ported from core.py


class OSVersionInfo(ctypes.Structure):
    _fields_ = (
        ("dwOSVersionInfoSize", ctypes.wintypes.DWORD),
        ("dwMajorVersion", ctypes.wintypes.DWORD),
        ("dwMinorVersion", ctypes.wintypes.DWORD),
        ("dwBuildNumber", ctypes.wintypes.DWORD),
        ("dwPlatformId", ctypes.wintypes.DWORD),
        ("szCSDVersion", ctypes.wintypes.WCHAR * 128),
    )

    def __init__(self, *args, **kwds):
        super(OSVersionInfo, self).__init__(*args, **kwds)
        self.dwOSVersionInfoSize = ctypes.sizeof(self)
        # future lint: disable=blacklisted-function
        kernel32 = ctypes.WinDLL(str("kernel32"), use_last_error=True)
        kernel32.GetVersionExW(ctypes.byref(self))


class OSVersionInfoEx(OSVersionInfo):
    """
    Helper function to return the results of the GetVersionExW Windows API call.
    It is a ctypes Structure that contains Windows OS Version information.

    Returns:
        class: An instance of a class containing version info
    """

    _fields_ = (
        ("wServicePackMajor", ctypes.wintypes.WORD),
        ("wServicePackMinor", ctypes.wintypes.WORD),
        ("wSuiteMask", ctypes.wintypes.WORD),
        ("wProductType", ctypes.wintypes.BYTE),
        ("wReserved", ctypes.wintypes.BYTE),
    )


async def _get_os_version_info() -> Dict[str, Any]:
    info = OSVersionInfoEx()
    ret = {
        "MajorVersion": info.dwMajorVersion,
        "MinorVersion": info.dwMinorVersion,
        "BuildNumber": info.dwBuildNumber,
        "PlatformID": info.dwPlatformId,
        "ServicePackMajor": info.wServicePackMajor,
        "ServicePackMinor": info.wServicePackMinor,
        "SuiteMask": info.wSuiteMask,
        "ProductType": info.wProductType,
    }
    return ret


async def load_service_pack(hub):
    info = await _get_os_version_info()
    if info["ServicePackMajor"] > 0:
        service_pack = "".join(["SP", str(info["ServicePackMajor"])])
        hub.corn.CORN["osservicepack"] = f"SP{service_pack}"
