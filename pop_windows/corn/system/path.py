import os


async def load_cwd(hub):
    """
    Current working directory
    """
    hub.corn.CORN["cwd"] = os.getcwd()
