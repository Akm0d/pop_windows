import logging
import socket
from typing import List

log = logging.getLogger(__name__)

# Possible value for h_errno defined in netdb.h
HOST_NOT_FOUND = 1
NO_DATA = 4


async def _get_fqdns(fqdn: str, protocol: int) -> List[str]:
    socket.setdefaulttimeout(1)
    try:
        result = socket.getaddrinfo(fqdn, None, protocol)
        return sorted({item[4][0] for item in result})
    except socket.gaierror as e:
        log.debug(e)
    return []


async def load_fqdns(hub):
    """
    Return all known FQDNs for the system by enumerating all interfaces and
    then trying to reverse resolve them (excluding 'lo' interface).
    """
    # Provides:
    # fqdns
    # fqdn_ip4
    # fqdn_ip6
    log.debug("loading fqdns")
    hub.corn.CORN["fqdn_ip4"] = await _get_fqdns(hub.corn.CORN["fqdn"], socket.AF_INET)
    hub.corn.CORN["fqdn_ip6"] = await _get_fqdns(hub.corn.CORN["fqdn"], socket.AF_INET6)
    hub.corn.CORN["fqdns"] = hub.corn.CORN["fqdn_ip4"] + hub.corn.CORN["fqdn_ip6"]
