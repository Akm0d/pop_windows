async def load_host(hub):
    hub.corn.CORN["host"], hub.corn.CORN["domain"] = hub.corn.CORN["fqdn"].partition(
        "."
    )[::2]
