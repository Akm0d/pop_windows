import socket


async def load_localhost(hub):
    hub.corn.CORN["localhost"] = socket.gethostname()


async def load_fqdn(hub):
    """
    Loads fqdn, hostname, domainname
    """
    # Provides:
    #   fqdn
    if not hub.corn.CORN.get("fqdn"):
        hub.corn.CORN["fqdn"] = socket.getfqdn() or "localhost"


async def load_socket_info(hub):
    # try socket.getaddrinfo to get fqdn
    if not hub.corn.CORN.get("fqdn"):
        try:
            addrinfo = socket.getaddrinfo(
                socket.gethostname(),
                0,
                socket.AF_UNSPEC,
                socket.SOCK_STREAM,
                socket.SOL_TCP,
                socket.AI_CANONNAME,
            )
            for info in addrinfo:
                # info struct [family, socktype, proto, canonname, sockaddr]
                if len(info) >= 4:
                    hub.corn.CORN["fqdn"] = info[3]
        except socket.gaierror:
            pass
