import re


async def _clean_value(key: str, val: str) -> str or None:
    """
    Clean out well-known bogus values.
    If it isn't clean (for example has value 'None'), return None.
    Otherwise, return the original value.

    NOTE: This logic also exists in the smbios module. This function is
          for use when not using smbios to retrieve the value.
    """
    if val is None or not val or re.match("none", val, flags=re.IGNORECASE):
        return None
    elif re.search("serial|part|version", key):
        # 'To be filled by O.E.M.
        # 'Not applicable' etc.
        # 'Not specified' etc.
        # 0000000, 1234567 etc.
        # begone!
        if (
            re.match(r"^[0]+$", val)
            or re.match(r"[0]?1234567[8]?[9]?[0]?", val)
            or re.search(
                r"sernum|part[_-]?number|specified|filled|applicable",
                val,
                flags=re.IGNORECASE,
            )
        ):
            return None
    else:
        # map unspecified, undefined, unknown & whatever to None
        if re.search(r"to be filled", val, flags=re.IGNORECASE) or re.search(
            r"un(known|specified)|no(t|ne)? (asset|provided|defined|available|present|specified)",
            val,
            flags=re.IGNORECASE,
        ):
            return None
    return val


async def load_bios_info(hub):
    biosinfo = await hub.exec.wmi.get("Win32_BIOS", 0)

    # bios name had a bunch of whitespace appended to it in my testing
    # 'PhoenixBIOS 4.0 Release 6.0     '
    hub.corn.CORN["biosversion"] = await _clean_value(
        "biosversion", biosinfo.Name.strip()
    )
    hub.corn.CORN["serialnumber"] = (
        await _clean_value("serialnumber", biosinfo.SerialNumber),
    )


async def load_time_info(hub):
    # http://msdn.microsoft.com/en-us/library/windows/desktop/aa394498(v=vs.85).aspx
    value = await hub.exec.wmi.get("Win32_TimeZone", 0, "Description")

    hub.corn.CORN["timezone"] = await _clean_value("timezone", value)
