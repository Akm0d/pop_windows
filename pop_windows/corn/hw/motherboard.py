import logging
import re

log = logging.getLogger(__name__)


async def _clean_value(key: str, val: str) -> str or None:
    """
    Clean out well-known bogus values.
    If it isn't clean (for example has value 'None'), return None.
    Otherwise, return the original value.

    NOTE: This logic also exists in the smbios module. This function is
          for use when not using smbios to retrieve the value.
    """
    if val is None or not val or re.match("none", val, flags=re.IGNORECASE):
        return None
    elif re.search("asset|manufacturer", key):
        # AssetTag0. Manufacturer04. Begone.
        if re.search(
            r"manufacturer|to be filled|available|asset|^no(ne|t)",
            val,
            flags=re.IGNORECASE,
        ):
            return None
    else:
        # map unspecified, undefined, unknown & whatever to None
        if re.search(r"to be filled", val, flags=re.IGNORECASE) or re.search(
            r"un(known|specified)|no(t|ne)? (asset|provided|defined|available|present|specified)",
            val,
            flags=re.IGNORECASE,
        ):
            return None
    return val


async def load_motherboard(hub):
    # http://msdn.microsoft.com/en-us/library/windows/desktop/aa394072(v=vs.85).aspx
    hub.corn.CORN["motherboard"] = {"product": None, "serial": None}
    try:
        motherboardinfo = await hub.exec.wmi.get("Win32_BaseBoard", 0)
        hub.corn.CORN["motherboard"]["product"] = motherboardinfo.Product
        hub.corn.CORN["motherboard"]["serial"] = motherboardinfo.SerialNumber
    except IndexError:
        log.debug("Motherboard info not available on this system")


async def load_system_info(hub):
    # http://msdn.microsoft.com/en-us/library/windows/desktop/aa394102%28v=vs.85%29.aspx
    systeminfo = await hub.exec.wmi.get("Win32_ComputerSystem", 0)
    hub.corn.CORN["manufacturer"] = (
        await _clean_value("manufacturer", systeminfo.Manufacturer),
    )
    hub.corn.CORN["productname"] = (
        await _clean_value("productname", systeminfo.Model),
    )
